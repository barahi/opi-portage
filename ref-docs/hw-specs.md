### Hardware Specification Orange PI Prime

> [Orange PI Prime Site](http://www.orangepi.org/OrangePiPrime/)


| CPU | H5 Quad-core Cortex-A53 |
| GPU | Mali450 GPU including dual Geometry Processors(GP) and quad Pixel Processors, Supports OpenGL ES 2.0 and OpenVG1.1, 3000Mpix/sec and 163Mtri/sec, Full scene over-sampled 4X anti-aliasing engine with no additional bandwidth usage |
| Memory (SDRAM) | 2GB DDR3 (shared with GPU) |
| Onboard Storage |TF card (Max. 32GB) / NOR flash(optional) |
| Onboard Network | 1000M/100M Ethernet RJ45 |
| Onboard WIFI+BT | 8723BS, IEEE 802.11 b/g/n,BT4.0 |
| Video Input | A CSI input connector Camera, Supports 8-bit YUV422 CMOS sensor interface, Supports CCIR656 protocol for NTSC and PAL, Supports SM pixel camera sensor, Supports video capture solution up to 1080p@30fps |
| Audio Input | MIC |
| Video Outputs | Supports HDMI output with HDCP, Supports HDMI CEC, Supports HDMI 30 function |
| Audio Outputs | 3.5 mm Jack, HDMI |
| Power Source | DC input can supply power |
| GPIO(1x3) pin | UART, ground. |
| USB 2.0 Ports | Three USB 2.0 HOST, one USB 2.0 OTG |
| Buttons | Power Button(SW2), Reset Button (SW4) |
| Low-level peripherals | 40 Pins Header,compatible with Raspberry Pi B+ |
| LED | Power led & Status led |
| Key | IR input, POWER,Reset |